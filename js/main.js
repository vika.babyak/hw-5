// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.


// Document obj
const Document = {
    title: "",
    body: "",
    footer: "",
    date: "",
    // Application obj
    Application: {
        // Title obj
        Title: {
            title: "",

            // fill Title method
            fillTitle: function (title) {
                this.title = title;             
            },
            /*showTitle: function () {
                document.write(`<h2>Приложение<h2><br><h3>${this.title}</h3>`);
            },*/
        },
        // Body obj
        Body: {
            body: "",

            // fill Body method
            fillBody: function (body) {
                this.body = body;
            },
            /*showBody: function () {
                document.write(`<p>${this.body}</p>`);
            },*/
        },
        // Footer obj
        Footer: {
            footer: "",

            // fill Footer method
            fillFooter: function (footer) {
                this.footer = footer;
            },
            /*showFooter: function () {
                document.write(`<p>${this.footer}</p>`);
            },*/
        },
        // Date obj
        Date: {
            date: "",

            // fill Date method
            fillDate: function (date) {
                this.date = date;
            },
            /*showDate: function () {
                document.write(`${this.date}`);
            },*/
        },

        // show Application method
        showApplication: function () {
            document.write(`<br><h4>Приложение<h4><h3>${this.Title.title}</h3><p>${this.Body.body}</p><p>${this.Footer.footer}</p>${this.Date.date}`);
        },
    }, 

    /*fillTitle: function (title) {
        this.title = title;
        document.write(this.title);
    },

    fillBody: (body) => {
        this.body = body;
    },

    fillFooter: (footer) => {
        this.footer = footer;
    },

    fillDate: (date) => {
        this.date = date;
    },*/

    // fill Document method
    fillDocument: (title, body, footer, date) => {
        this.title = title;
        this.body = body;
        this.footer = footer;
        this.date = date;
    },

    // show Document method
    showDocument: () => {
        document.write(`<h3>${this.title}</h3><p>${this.body}</p><p>${this.footer}</p>${this.date}`);
    },
}

// fill Document
Document.fillDocument(prompt("Заголовок документа", "Lorem ipsum"), prompt("Тело документа", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque alias mollitia totam ab reiciendis molestias illum vero fuga maxime? Voluptate, incidunt? Impedit inventore atque deleniti veniam delectus neque sequi aliquam? Fuga repellat aperiam quo, itaque excepturi culpa, temporibus quidem eaque ad reprehenderit aut, cumque esse unde assumenda. Debitis, deleniti, expedita amet dolorum iusto ipsum laborum, incidunt nemo minus doloribus corporis? Consequuntur ea laudantium quae, accusamus excepturi officia neque, error sed architecto numquam vitae soluta sit voluptate illum ut libero? Deserunt expedita aliquam est quae, eveniet saepe impedit eligendi aut quidem!"), prompt("Футер документа", "John Doe"), prompt("Дата документа", new Date()));

// fill Title
Document.Application.Title.fillTitle(prompt("Заголовок приложения", "Lorem"));

// fill Body
Document.Application.Body.fillBody(prompt("Тело приложения", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque beatae culpa corporis explicabo numquam harum, quae molestiae quibusdam? Facilis voluptas voluptatem, neque reiciendis ab tempora velit omnis dolor a fugit.Necessitatibus molestias, aliquid voluptatum odio reprehenderit atque est voluptates ullam laborum enim. Dolorum iusto quisquam dignissimos reiciendis temporibus ipsa maiores, ut consectetur ea unde hic natus optio voluptas ipsum. Quasi."));

// fill Footer
Document.Application.Footer.fillFooter(prompt("Футер приложения", "Alice Smith"));

// fill Date
Document.Application.Date.fillDate(prompt("Дата приложения", new Date()));

Document.showDocument();

Document.Application.showApplication();